


/**
 * 拼装附加条件数据,并生成span
 * @param additionals
 * @returns {string}
 */
function getAdditionals(additionals){
    var html = "";
    additionals.forEach(function (value) {
        html += '<span class="label bgDarkOrange additional" title="附加条件">'+value+'</span>';
    });
    return html;
}
/**
 * 隐藏user id输入框,只显示用户id
 */
function userIdReadonly(id){
    $("#personalId").attr("readonly","readonly");
    $(".inputOk,#personalId,#personalSave")
        .css("background","#2F4F4F").css("color","#2F4F4F");
    $("#personalId").css("text-align","right").css("color","white !important");
    $("#personalSave").text(id).css("color","white");
}

/**
 * user id输入框,恢复原来样式
 */
function userIdReadonlyReset(){
    $("#personalId").removeAttr("readonly").css("color","#444444").focus();
    $(".inputOk,#personalId").css("background","white");
    $("#personalSave").text('确定').css("color","white").css("background","#428bca");
    $("#personalId").css("text-align","left").css("color","white !important");
}


/**
 * 检测正则是否匹配成功
 * @param regexp
 * @param index
 * @returns {boolean}
 */
function check(regexp,index){
    var successTxt = $("#success_"+index).val();
    var errorTxt = $("#error_"+index).val();
    var prompt = $("#prompt_"+index);
    var inputWrapper = $("#input_"+index).parent().removeClass("borderGray").removeClass("borderGreen");
    if(!regexp){
        prompt.html("");//clear
        inputWrapper.addClass("borderRed");
        return false;
    }
    prompt.html("");//clear

    //正则合法性检查
    try {
        new RegExp(regexp, 'g')
    }catch(e){
        prompt.html('<span class="label bgRed">'+e+'</span>');
        inputWrapper.addClass("borderRed");
        return false;
    }

    var error_count = 0;
    //检索出匹配错误的文本
    if(successTxt){
        var list = successTxt.split('\n');
        for (var i=0;i<list.length;i++){
            if(!new RegExp(regexp,'g').test(list[i])){
                prompt.append('<span class="label bgRed">'+transfer(list[i])+'</span>');
                error_count++;
            }
        }
    }
    if(errorTxt){
        var list = errorTxt.split('\n');
        for (var i=0;i<list.length;i++){
            if (new RegExp(regexp, 'g').test(list[i])) {
                prompt.append('<span class="label bgRed">' + transfer(list[i]) + '</span>');
                error_count++;
            }
        }
    }
    if(error_count<1){
        prompt.html('<span class="label bgGreen">匹配成功</span>');
        inputWrapper.removeClass("borderRed").addClass("borderGreen");
        return true;
    }
    inputWrapper.addClass("borderRed");
    return false;
}

/**
 * 转义处理
 * @param regexp
 * @returns {string}
 */
function transfer(regexp){
    return regexp.replace(/</ig,'&lt;').replace(/>/ig,'&gt;').replace(/ /ig,'<span class="bgGray">&nbsp;</span>');
}

/**
 * show regexp
 * @returns {boolean}
 */
function showRegexpConfirm() {
    var msg = "真不行啦?????????????？";
    if (confirm(msg)==true){
        return true;
    }else{
        return false;
    }
}

/**
 * 将缓存的正则显示到界面
 */
function renderByCache(userInfo,libs){
    if(!userInfo||!userInfo.data) return;
    console.log("[renderByCache userId,libsLen]: ",userInfo.id,libs.length)
    var regexps = userInfo.data.regexps;
    if(libs&&libs.length>0&&regexps){
        for(var key in regexps){
            $("#input_"+key).val(regexps[key]);
            check(regexps[key],key);
        }
    }
}

/**
 * 保存用户信息
 * @param id
 */
function saveUserInfoToLocal(userInfo) {
    return new Promise(function(resolve,reject){
        try {
            localStorage.setItem("userInfo", JSON.stringify(userInfo) );
            resolve(userInfo);
        }catch(e){
            reject("[saveUserInfoToLocal: failed");
        }
    })
}

/**
 * 查询用户id,远程抓取用户信息,显示并更新缓存
 * @param id
 */
function loadUserInfoFromLocal() {
    return new Promise(function(resolve,reject){
        var userInfo = localStorage.getItem("userInfo");
        if(userInfo){
            userInfo = JSON.parse(userInfo);
            if(userInfo&&userInfo.id){
                resolve(userInfo);
            }else{
                reject(userInfo);
            }
        }else{
            reject("未找到用户信息本地缓存");
        }
    });
}

/**
 * 查询远程用户正则数据
 * @param id
 * @returns {Promise}
 */
function queryRemote(path){
    return new Promise(function(resolve,reject){
        wilddog.sync().ref(path)
            .once("value")
            .then(function(snapshot) {
                if(snapshot.exists()){
                    resolve(snapshot.val());
                }else{
                    reject("未查询到数据")
                }
            });
    });

}

/**
 * pv
 * @returns {Promise<T>}
 */
function pvCountPlus(){
    var daySum = {};
    var promise =  new Promise(function(resolve,reject){
        var sumRef = wilddog.sync().ref("/up/sum");
        sumRef.transaction(function (currentValue) {
            daySum.sum = Number(currentValue || 0) + 1
            return daySum.sum;   // 判断计数器是否为空或者是自增加
        }).then(function(data){
            resolve(data);
        }).catch(function(error){
            reject(error)
        });
    }).then(function(value){
        return new Promise(function(resolve,reject){
            var dayRef = wilddog.sync().ref("/up/days/"+(new Date()).pattern("yyyy-MM-dd"));
            dayRef.transaction(function (currentValue) {
                daySum.day = Number(currentValue || 0) + 1
                return daySum.day;   // 判断计数器是否为空或者是自增加
            }).then(function(data){
                resolve(daySum);
            }).catch(function(error){
                reject(error);
            });
        })
    });

    return promise;
}

/**
 * 添加正则，并存储到远程
 * @param lib_regexp
 */
function addRegexpToRemote(lib_regexp){
    var ref = wilddog.sync().ref("/lib/")
    ref.once("value", function(snapshot) {
        var arr = snapshot.val();
        var newIndex = arr.length;
        lib_regexp['id'] = String(newIndex);
        wilddog.sync().ref("/lib/" + newIndex)
            .update(lib_regexp)
            .then(function (info) {
                console.log("[addRegexpToRemote]: ","/lib/" + newIndex, lib_regexp);
                alert("保存成功!");
            }, function (error) {
                console.log(error);
                alert("保存失败!");
            });
    });
}

function saveRegexp(index){
    var regexp = $("#input_" + index).val();
    if (!check(regexp, index)) {
        alert("正则不正确,无法保存!");
        return;
    }
    var personalId = $.trim($("#personalSave").text());
    if (personalId == "确定") {
        alert("请先输入用户id");
        userIdReadonlyReset();
        return;
    }
    if (index == 0) {//用户自造正则,需要保存到公共正则库 /lib
        var title = prompt('给你的正则起个名字吧：');
        if(title == null){
            //('你取消了输入！');
        }else if($.trim(title)==''){
            alert("标题不能为空")
        }else{
            var additionals = prompt('附加条件以,分隔：\n如：数字,大于0,开头不能为0');
            if(additionals == null){
                //('你取消了输入！');
            }else if(additionals == ''){
                alert("提示不能为空")
            }else{
                additionals = additionals.split(/[,，，]+/);

                var success_text = $("#success_"+index).val()||"";
                if(success_text=='这里自己填不匹配的测试文本'){
                    success_text = '';
                }
                var error_text = $("#error_"+index).val()||"";
                if(error_text=='这里自己填不匹配的测试文本'){
                    error_text = '';
                }
                var libRegexp = {
                    'title':title,
                    'additional':additionals,
                    'success':success_text.split("\n"),
                    'error':error_text.split("\n"),
                    'answer':regexp,
                    'author':personalId
                }
                addRegexpToRemote(libRegexp);
            }
        }

    }else{//用户私有正则,存在到/users/{userId}/regexps/{index}
        var obj = {};
        obj[index] = regexp;
        wilddog.sync().ref("/users/" + personalId +"/regexps")
            .update(obj)
            .then(function (info) {
                console.log("[saved]: ","/users/" + personalId +"/regexps/"+index, regexp);
                alert("保存成功!");
            }, function (error) {
                console.log(error);
                alert("保存失败!");
            });
    }
}

function appendRegexpCard(index,v){
    $("#regexps").append(
        '<li class="regexp" id="li_' + index + '" >'
        + '<div class="card">'
        + '<div class="title_wrapper">'
        + '<span class="id">' + v["id"] + '</span>'
        + '<span>.</span>'
        + '<span class="title">' + v["title"] + '</span>'
        + '<span id="answerBtn_' + index + '" class="answerBtn" title="正则创建者">'+v["author"]+'</span>'
        + '</div>'
        + '<div class="additional_wrapper">'
        + getAdditionals(v["additional"])
        + '</div>'
        + '<div>'
        + '<div class="input_wrapper">'
        + '<div class="right inputBtnWrapper circle borderGray">'
        + '<input id="input_' + index + '" type="text" class="regexp_text" title="请输入正则" placeholder="请输入正则"/>'
        + '<div id="input_save_' + index + '" >保存</div>'
        + '</div>'
        + '<span id="answerShow_' + index + '" class="answerShow">&nbsp;</span>'
        + '</div>'
        + '<div class="example">'
        + '<div class="example_inner">'
        + '<textarea id="success_' + index + '" class="borderGreen" title="要匹配的文本">' + v["success"].join("\n") + '</textarea>'
        + '</div>'
        + '<div class="example_inner">'
        + '<textarea id="error_' + index + '" class="borderRed" title="不要匹配的文本">' + v["error"].join("\n") + '</textarea>'
        + '</div>'
        + '<div id="prompt_' + index + '" class="prompt_wrapper"></div>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '</li>'
    );
}
/**
 * 主函数
 * @param wilddog
 * @param lib
 */
function mainFn(wilddog,libs) {
    console.log("[mainFn]: libsLen",libs.length);
    return  new Promise(function(resolv,reject){
        libs.map(function (v, index, array) {
            appendRegexpCard(index,v);
            //input实时监听，并检查正则是不正确
            (function(i){
                var timer
                $("#input_" + index).on('input propertychange', function () {
                    var regexp = $(this).val();
                    if (timer) clearTimeout(timer);
                    timer = setTimeout(function () {
                        // render(regexp,index,v);
                        check(regexp, i, v);
                    }, 300);
                });

                //显示答案
                $("#answerBtn_" + index).click(function () {
                    alert("此正则由"+$(this).text()+"分享!");
                    // if (showRegexpConfirm()) {
                    //     alert("不行也不给你答案^_^,在群里问(434252251)")
                    //     //$("#answerShow_"+index).html(transfer(v['answer']));
                    // }
                });

                $('#input_'+index+',#input_save_' + index).bind('keypress',function(event) {
                    if (event.keyCode == "13") {
                        saveRegexp(index);
                    }
                });
                //保存自己的正则
                $("#input_save_" + index).click(function () {
                    saveRegexp(index);
                });

            })(index);
        })//-- end libs for;
        resolv(libs)
    });// return promise
} //-- end main