
function isFunction(func) {
    return typeof func === 'function';
}
function isObject(obj) {
    return typeof obj === 'object';
}
function isArray(arr) {
    return Object.prototype.toString.call(arr) === '[object Array]';
}

var PENDING = 'pending';
var FULFILLED = 'fulfilled';
var REJECTED = 'rejected';
function Promise(executor){
    if (typeof executor != 'function') {
        throw new Error('Promise resolver ${executor} is not a function');
    }
    var self = this;
    self.resolveCbs = [];
    self.rejectCbs = [];
    self.value = undefined;
    self.states = ['pending','fulfilled','rejected'];
    self.state = PENDING;

    function resolve(value){
        setTimeout(function(){
            if(self.state === PENDING) {
                self.state = FULFILLED;
                self.value = value;
                for(var i = 0; i<self.resolveCbs.length;i++){
                    self.value = self.resolveCbs[i](self.value);
                }
            }
        });
    }

    function reject(value){
        setTimeout(function() {
            if (self.state === PENDING) {
                self.state = REJECTED;
                self.value = value;
                for(var i = 0; i<self.rejectCbs.length;i++){
                    self.value = self.rejectCbs[i](self.value);
                }
            }
        });
    }

    try{
        executor&&executor(resolve, reject);
    }catch(e){
        self.state = REJECTED;
        self.value = e;
    }
}

Promise.prototype.then = function (resolveCb,rejectCb) {
    var self = this;

    return new Promise(function(resolve,reject){
        if(self.state === PENDING) {
            self.resolveCbs.push(handler);
            self.rejectCbs.push(rejectHandler);
        }else if(self.state === FULFILLED) {
            handler(self.value);
        }else if(self.state === REJECTED) {
            rejectHandler(self.value);
        }

        function handler(value){
            try {
                var ret = typeof resolveCb === 'function' ? resolveCb(value) : value;//resolve as value
                if (Promise.isPromise(ret)) {//as promise
                    ret.then(function (value) {
                        resolve(value);
                    }, function (reason) {
                        reject(reason);
                    });
                } else {
                    resolve(ret);
                }
            }catch (e){
                reject(e);
            }
        }

        function rejectHandler(reason){
            var reason = typeof rejectCb === 'function'? rejectCb(reason):reason;
            reject(reason);
        }
    });
}

Promise.prototype.catch = function(rejectCb){
    return this.then(null,rejectCb);
}

Promise.isPromise = function(unknown){
    return unknown && unknown["toString"] && unknown.toString() === "[object Promise]";
}

Promise.prototype.isPromise = function(){
    return Promise.isPromise(this);
}

Promise.prototype.toString = function(){
    return "[object Promise]";
}

Promise.ajax = Promise.prototype.ajax = function(param, success, error, change) {
    param = param || {};
    var type = (param.type || 'GET').toUpperCase();
    var async = param.async || true;
    success = success || param['success'];
    error = error || param['error'];
    change = change || param['change'];


    var paramGET = [];
    for(var key in param.data){
        paramGET.push(key+'='+param.data[key]);
    }
    var paramData = paramGET.join('&');
    //请求的5个阶段，对应readyState的值
    //0: 未初始化，send方法未调用；
    //1: 正在发送请求，send方法已调用；
    //2: 请求发送完毕，send方法执行完毕；
    //3: 正在解析响应内容；
    //4: 响应内容解析完毕；


    var xhr = window.XMLHttpRequest? new XMLHttpRequest():new ActiveXObject('Microsoft.XMLHTTP'); //创建一个ajax对象

    if(type === 'GET'){
        var url = param['url'];
        if(url.indexOf('?')>-1 && url.indexOf('=')>-1) {//url中已有参数
            if(url.lastIndexOf('&') == url.length-1){//&为最后一个
                url += paramData;
            }else{
                url += '&'+paramData;
            }
        }else{//url中没有参数
            url += "?"+paramData;
        }
        xhr.open(type, url, async);
        xhr.send(null);
    }else if(type === 'POST'){
        xhr.open(type, param['url'], async);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded;charset=utf-8');
        xhr.send(param.data || null);
    }else{
        throw new Error("The Type Error of method");
    }

    xhr.onreadystatechange = function(event) { //对ajax对象进行监听
        if (change && typeof change === 'function' && false === change(xhr.readyState, event)) {
            return;
        }
        if (xhr.readyState == 4) { //4表示解析完毕
            if (xhr.status == 200) { //200为正常返回
                success && success(xhr.responseText);
            } else {
                error && error(xhr.status, xhr);
            }
        }
    };
}

Promise.get = function(url,data,async){
    var param = url; //url as param object {},data and async will not be used
    if(typeof url === 'string'){// url just url,data async can be used
        param = {
            url:url,
            data:data,
            async:async||true
        }
    }
    return new Promise(function(resolve,reject){
        Promise.ajax(param,function (data) {
                resolve(data);
            },function(status,xhr){
                reject(status,xhr)
            }
        );
    });
}

Promise.post = function(url,data,async){
    return new Promise(function(resolve,reject){
        Promise.ajax({
                url:url,
                type:'POST',
                data:data,
                async:async||true
            },function (data) {
                resolve(data);
            },function(status,xhr){
                reject(status,xhr)
            }
        );
    });
}